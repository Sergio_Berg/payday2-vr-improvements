# Changelog

## V0.1.7.0:
- Add Russian translations (Thanks, Sergio)

## V0.1.6.3:
- Remove BLT hook DLL from automatic updates
- Warn user if the mod's filename is incorrect and will cause issues while updating

## V0.1.6.2:
- Fix crash on startup caused by v0.1.6.1 and extremely inadequate testing on my part
- Note this version's mod.txt says v0.1.6.1 - I forgot to update it

## V0.1.6.1:
- Add redout effect (disabled by default), fading screen to red as your health runs low - See #21

## V0.1.6:
- Add option to disable locomotion
- Warn the user if an outdated IPHLPAPI.dll is found

## V0.1.5.3:
- Add mod icon

## V0.1.5.2:
- Split camera and control options into two different menus

## V0.1.5.1:
- Fix crash when jumping while downed - See #18

## V0.1.5:
- Adds automatic updates

## V0.1.4:
- Implement controller-relative (Onward-like) movement: #8
- Fix major movement bug: #9
- Add thumbstick/trackpad-based rotation (smooth and snapping)

## V0.1.3:
- Add jumping support.
- Fix issue #4 preventing users from moving while in casing mode (not masked up).
- Adds configuration options for what the camera does when you put your head into a wall, along with defaults far better suited to locomotion movement.

## V0.1.2:
- Add deadzone slider (mainly for Vive users)

## V0.1.1:
- Add sticky sprinting checkbox (default on)

## V0.1.0:
- Initial Release

